import './styles.css'

import F7, { Template7, Dom7 } from 'framework7/dist/framework7.esm.bundle.js'

import pkg from '../package'

console.log(`Built for target: ${__BUILD_TARGET__}`) 

const config = {
  requestUrl: 'https://randomuser.me/api/',
  requestParams: {
    inc: 'name,email,phone,picture',
    nat: 'uk,dk,fr,gb',
    results: 1000
  }
}

const compItemTemplate = Template7.compile(Dom7('#list-item-template').html())

const app = new F7({
  root: '#app',
  name: pkg.displayName,
  id: pkg.name,
  panel: {
    swipe: 'left',
  },
  routes: [
    {
      path: '/',
      url: 'index.html',
    },
    {
      path: '/detail/:index',
      componentUrl: 'detail.component',
    }
  ],
  data () {
    return {
      employees: [],
      env: {
        mode: __MODE__,
        cordova: __CORDOVA__
      }
    }
  },
  on: {
    init () {
      console.log(`App Init in ${__MODE__} mode ${ __CORDOVA__ ? 'with' : 'without' } Cordova`) 
      if (__CORDOVA__) {
        document.addEventListener('pause', e => {
          console.log('App pause')
        }, false)
        document.addEventListener('resume', e => {
          console.log('App resume')
        }, false)
      }

      Dom7('.fab').on('click', () => {
        this.methods.refresh()
      })
    },
    pageInit (e) {
      if (this.data.employees.length === 0) {
        this.methods.refresh()
      }
    }
  },
  methods: {
    refresh () {
      const capitalize = name => name.slice(0, 1).toUpperCase() + name.slice(1)

      app.methods.fetchData().then(json => {
        app.data.employees = json.results
        app.data.employees.forEach((employee, index) => {
          const { title, first, last } = employee.name
          employee.name = { title: title, first: capitalize(first), last: capitalize(last) }
          employee.index = index
        })
//        if (!app.data.config.requestParams.seed) {
//          app.data.config.requestParams.seed = json.info.seed
//        }
        app.methods.populateList()
      }).catch(err => {
        app.dialog.alert(err, 'Error')
        console.error(err)
      })
    },
    fetchData () {
      app.fab.close('.fab')
      app.preloader.show()
    
      return new Promise((resolve, reject) => {
        app.request.get(config.requestUrl, config.requestParams,
          (data, status) => {
            app.preloader.hide()
            app.fab.open('.fab')

            if (status != 200) {
              reject(`Server responded with status ${status}`)
            } else {
              resolve(data) 
            }
        }, (xhr, status) => {
          app.fab.open('.fab')
          app.preloader.hide()

          reject(status)
        }, 'json')
      })
    },
    populateList () {
      app.virtualList.create({
        el: '.virtual-list',
        items: app.data.employees,
        searchAll: function (query, items) {
          var found = []
          items.map((item, index) => {
            if (Object.values(item.name).join(' ').toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(index)
          })
          return found
        },
        itemTemplate: compItemTemplate,
        height: app.theme === 'ios' ? 63 : 73,
        on: {
          vlItemsAfterInsert (vl, fragment) {
            app.lazy.create('.page')
          }
        }
      })
    }
  },
  initOnDeviceReady: true,
  pushState: true
})

app.view.create('.view-main', { url: '/' })
