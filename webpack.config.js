const path = require('path')
const pkgInfo = require('./package')

const webpack = require('webpack')

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

const HtmlPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')
const MinifyPlugin = require('babel-minify-webpack-plugin')

// const CspPlugin = require('csp-webpack-plugin')
// const ExtractTextPlugin = require('extract-text-webpack-plugin')

const config = {
  outputDir: path.resolve(__dirname, 'www'),
  srcDir: path.resolve(__dirname, 'src'),
  appEntry: 'index.js',
}

module.exports = env => {

  if (env.target) {
    if ((['android', 'ios', 'browser']).indexOf(env.target.toLowerCase()) === -1) {
      throw Error('Invalid target platform: ' + env.target)
    }
  } 

  const _exports_ = {
    entry: {
      vendor: ['babel-polyfill', 'framework7'],
      app: path.join(config.srcDir, config.appEntry)
    },

    context: config.srcDir,

    output: {
      path: config.outputDir,
      filename: '[name].[hash].js'
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: [ path.resolve(__dirname, 'node_modules') ],
          loader: 'babel-loader'
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          use: [
            { loader: 'file-loader', options: { name: 'fonts/[name].[ext]' } }
          ]
        }
          /*
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
                { loader: "css-loader", options: { sourceMap: true } },
                { loader: "sass-loader", options: { sourceMap: true } }
            ]
          })
        }
        */
      ]
    },

    devServer: {
      port: 8080,
      historyApiFallback: true,
      noInfo: true,
      overlay: true,
      hot: true
    },

    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },

    performance: {
      hints: false
    },

    devtool: '#cheap-eval-source-map',

    resolve: {
      modules: [path.resolve(__dirname, "node_modules")]
    },

    plugins: [
      new webpack.DefinePlugin({
        '__CORDOVA__': env.cordova,
        '__BUILD_TARGET__': env.target ? JSON.stringify(env.target.toUpperCase()) : undefined
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: Infinity
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'manifest',
      }),
      new webpack.SourceMapDevToolPlugin({
        filename: '[name].[hash].js.map',
        exclude: ['vendor', 'manifest']
      }),
      new BundleAnalyzerPlugin({
        reportFilename: '.report.html',
        analyzerMode: 'static',
        openAnalyzer: false
      }),
      new HtmlPlugin({
        title: pkgInfo.displayName,
        template: path.join(config.srcDir, 'index.html'),
        filename: 'index.html',
        minify: false,
        chunksSortMode: 'dependency',
        cordova: env.cordova,
        csp: false
      }),
      new CopyPlugin([
        { from: config.srcDir, to: config.outputDir }
      ], { test: /\.component$/ }),
      new CleanPlugin(config.outputDir, { exclude: ['.gitignore'] }),
      new MinifyPlugin(),
//    new ExtractTextPlugin('css/[name].css'),
/*
      new CspPlugin({
        'object-src': '\'none\'',
        'base-uri': '\'self\'',
        'script-src': ['\'unsafe-inline\'', '\'self\'', '\'unsafe-eval\''],
        'worker-src': ['\'self\'','blob:']
      })
*/
    ]
  }

  if (env.prod) {
    console.log('Production mode')

    _exports_.devtool = '#eval-source-map'

    _exports_.plugins.push(new webpack.DefinePlugin({ '__MODE__': JSON.stringify('PROD') }))
    _exports_.plugins.push(new MinifyPlugin())
  
  } else if (env.dev) {
    console.log('Development mode')

    _exports_.plugins.push(new webpack.DefinePlugin({ '__MODE__': JSON.stringify('DEV') }))
  } else {
    _exports_.plugins.push(new webpack.DefinePlugin({ '__MODE__': undefined }))
  }
  
  if (env.cordova) {
    if (!env.target) {
      throw Error('You must specify target platform for building via --target key')
    }

    console.log('Building for Cordova ' + env.target)

    //_exports_.entry.vendor.push(path.resolve('platforms', env.target, 'platform_www/cordova.js'))
  }

  return _exports_

}
